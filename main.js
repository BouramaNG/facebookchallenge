const show = () => {
    const form = document.querySelector("#signup")
    form.classList.toggle("show")
    document.querySelector(".layer").style.display="block"
   
}

document.querySelector('.signup_form').addEventListener('submit', function(e) {
    e.preventDefault(); // Empêche la soumission du formulaire

    // Liste des champs à vérifier
    var fields = ['prenom', 'nom', 'email', 'motdepasse'];

    fields.forEach(function(field) {
        var input = document.getElementById(field);
        var error = document.getElementById(field + '-error');

        if (input.value.trim() === '') {
            // Si le champ est vide, affichez l'erreur
            error.style.display = 'block';
        } else {
            // Sinon, cachez l'erreur
            error.style.display = 'none';
        }
    });
});
